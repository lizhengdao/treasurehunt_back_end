package com.example.demo.Registration;

public interface LoseConditionRegistration<USER> {

    Integer loseCondition(String userName) throws Exception;

}
